<footer class="main">
    <div class="container">
        <div class="row justify-content-lg-between">
            <div class="col-auto d-none d-md-block">
                <a class="branding"><img src="images/logo.svg" width="200" height="90"></a>
            </div><!-- end col -->
            <div class="col-12 col-md-9">
                <ul class="nav nobar">
                    <li><a class="tif-btn btn-sm red" href="full-report.pdf">Full-Report.pdf</a></li>
                    <li><a class="tif-btn btn-sm red" href="private-hospitals.pdf">private-hospitals.pdf</a></li>
                    <li><a class="tif-btn btn-sm red" href="air-ambulance.pdf">air-ambulances.pdf</a></li>
                    <li><a class="tif-btn btn-sm red" href="premature-babies.pdf">premature-babies.pdf</a></li>
                </ul>
                <ul class="nav">
                    <li><a href="https://www.tifgroup.co.uk/terms-conditions/" target="_blank" rel="noreferrer">Terms &amp; Conditions</a></li>
                    <li><a href="https://www.tifgroup.co.uk/privacy/" target="_blank" rel="noreferrer">Privacy Policy</a></li>
                    <li><a href="https://www.tifgroup.co.uk/cookie-policy/" target="_blank" rel="noreferrer">Cookie Policy</a></li>
                    <li><a href="https://www.tifgroup.co.uk/sitemap/" target="_blank" rel="noreferrer">Sitemap</a></li>
                    <li><a href="report.pdf" target="_blank" rel="noreferrer">Download the report</a></li>
                </ul>
                <p>tifgroup is a trading name of Travel Insurance Facilities plc which is authorised and regulated by the Financial Conduct Authority FRN 306537 registered office address is 2nd Floor, 1 Tower View, Kings Hill, West Malling, Kent, ME19 4UY, United Kingdom.</p>
                <p>Union Reiseversicherung AG is authorised in Germany by BaFin and regulated in the United Kingdom by the Financial Conduct Authority and in The Republic of Ireland by the Insurance Regulator.</p>
                <p>© Travel Insurance Facilities Plc 2018, All rights reserved</p>
            </div><!-- end col -->
        </div><!-- end row -->
    </div>
</footer>

<script src="js/vendor/modernizr-3.6.0.min.js"></script>
<script src="js/jquery.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/rellax.min.js"></script>
<script src="js/custom.js"></script>