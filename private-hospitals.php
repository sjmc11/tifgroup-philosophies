<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TIF Group | Private Hospitals</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link href="https://fonts.googleapis.com/css?family=Oswald:500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/master.css">
    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

        })(window,document,'script','dataLayer','GTM-P7RZ5VH');</script>

    <!-- End Google Tag Manager -->
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<?php include('header.php'); ?>

<section class="jumbo red black layr animated fadeIn">
    <a class="scroll-arrow trans" href="#more"><img src="images/scroll-arrow.png"></a><!-- end scroll arrow -->
    <div class="background" style="background-image: url('images/private-hero.jpg');"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-1">
            <h1 class="mb-3 animated fadeInDown">Private<br>Hospitals</h1>
            <div class="animated fadeInUp">
                <p>Our position on where customers receive medical treatment is based solely on optimal care.</p>
                <a class="tif-btn red mt-3" href="#more">Learn more</a>
            </div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="callout" style="background-image: url('images/private2.jpg');" id="more">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">Our position on where customers receive medical treatment is based solely on optimal care.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial reverse small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/private3.jpg" class="picture layr" data-rellax-speed="-0.5" style="top:15%;max-width: 400px;">
        <img src="images/private4.jpg" class="picture layr" data-rellax-speed="-0.5" style="top: 40%;right:3rem;">
        <div class="text-cont layr mt-2" data-rellax-speed="1.5">
            <p>In the United Kingdom both Private and Public Hospitals are regulated in the same way by the same entity. The facilities and standards of care is inspected regularly and staff are employed based on qualifications and experience –</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2">
            <h4 class="text-left pl-lg-4 pl-xl-5">There is no such regulation in place in Private Clinics in some parts of the World.</h4>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2.5">
            <p>In our experience, it is common to see customers receiving unnecessary treatment and investigations in private clinics as a way to increase income as opposed to clinical requirement. This practise is not only wrongful, but can also be dangerous. Treatment in public hospitals, is driven only by the clinical needs of a patient.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2.5">
            <p>The reality associated with this, is that patients at a private facility overseas could have serious medical situations overlooked or misdiagnosed due to lack of appropriate diagnostics or expertise available.  We have cases that prove this and cases where unfortunately the clinical outcome has been life changing.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/private5.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">We have had a known case where a doctor qualified to GP level was prepared to undertake orthopaedic surgery with no training to do so.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial stance small-gap align-left">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/private6.jpg" class="picture layr" data-rellax-speed="2" style="top: 25%;right: 4rem;">
        <div class="text-cont layr" data-rellax-speed="1">
            <p>Private hospitals overseas are renowned for over-treating, over-examining and over-charging holidaymakers</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h4 class="pl-5 text-left">They are financially motivated by your treatment.</h4>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p>An indication as to whether a facility has a financial incentive over you and your care is often demonstrated as soon as you step in the door. Some private hospitals will demand an upfront payment, credit card or your passport before even considering a medical assessment – regardless of how injured or ill you are when you arrive.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/private7.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3 mg-xl-4">We have had known cases of nursing staff withdrawing much needed pain relief from a patient in order to extract money from them.</h2>
            <h2 class="mb-0">This is illegal, immoral and an absolute breach of duty of care.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="1">
            <p class="pl-0">For us, access to the best available care for our customers in a medical situation has and will always come before convenience or cost. Choosing style over substance in a medical situation can have significant and severe consequences to your health. –</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h4 class="text-left">Quality of clinical care must override all other considerations.</h4>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">Many private clinics offer hotel like luxury and comfort facilities, making the facility seem ‘better’. It is however a reality, that the facility itself, may not be clinically appropriate despite the superior façade, and that this could be to detriment of customers health and clinical outcome.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/private8.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">Convenience shouldn’t take priority over best medical outcome.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial reverse align-right small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/private11.jpg" class="picture layr" data-rellax-speed="-0.5" style="top:15%;max-width: 600px;">
        <div class="text-cont layr mt-2" data-rellax-speed="1.5">
            <p class="pl-0">We have found that some private clinics have set up commercial contracts with third parties, such as hotels, which then advise holidaymakers to go to those clinics rather than to more appropriate and better equipped public hospitals for emergency treatment.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2">
            <p class="text-left pl-0">An automatic referral of a patient to a private clinic in this way, without an assessment as to the best facility for the treatment of the patient - Removes patient choice.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2">
            <p class="text-left pl-0">It can place the patient in an extremely difficult position when presented to the private clinic in urgent need of medical treatment which in fact is either only available, or more readily available, at a public hospital.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2.5">
            <h4 class="text-left">This activity is clearly immoral and unethical.</h4>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2.5">
            <p class="text-left pl-0">We are funding an investigation into the corrupt practices of hoteliers and private medical facilities, whereby patients are transferred by medically unqualified hotel staff to private medical facilities pursuant to contractual arrangements which reward hoteliers for their ‘referrals’.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2.5">
            <h4 class="text-left">We are quite simply spending money with a view to pursue criminal proceedings in order to protect the interests of you, the UK Holidaymaker.</h4>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/private9.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">It is important to mention that not all private facilities will operate in the same way, the point is, you are unlikely to know, therefore the risk in our experience is that it is not worth the gamble.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial small-gap" style="padding-bottom: 4rem;">
    <div class="clearfix"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="1">
            <p class="pl-0">When your insurer advises you to transfer to a public hospital it is not travel insurers trying to get out of paying your claim – It is simply to ensure you receive the best possible care and to mitigate the risk of clinical or emotional harm.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">For us at tifgroup it’s about preventing you from having to gamble the quality of care you receive.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h4 class="text-left">We are and will continue to challenge the practice of private clinics that put patients at risk for financial gain.</h4>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="editorial reverse align-right ambulance pb-5">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/private1.jpg" class="picture layr" data-rellax-speed="2" style="top: 15rem;">
        <div class="text-cont layr" data-rellax-speed="1">
            <p>Most UK travel insurers only cover private medical care where there are no adequate state facilities available. Whilst most UK travel insurers carry this exclusion around the use of private facilities, not all firms are prepared to actually enforce this exclusion for fear of complaints and perceived customer dissatisfaction.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p>We, tifgroup, operate differently and are passionate about making a stand, with very good reason and that is the best possible chance of good clinical outcome for the customer.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>


<section class="editorial small-gap" style="padding-top: 2rem!important;">
    <div class="clearfix"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="1">
            <a class="tif-btn red mt-3" href="private-hospitals.pdf" target="_blank">Read this as a PDF</a>
        </div>
        <div class="text-cont layr" data-rellax-speed="1">
            <h4 class="text-left">A <span style="color:red;">true</span> story</h4>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">A five-year-old was taken to a private hospital in a popular Spanish resort after complaining of stomach pain. The private hospital diagnosed gastroenteritis placed onto an IV saline solution for rehydration and declared the child would be fine in a few days.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">We, <span style="color:red;">tifgroup</span> advised that the child should be transferred to a children’s unit attached to a university teaching hospital but the private clinic criticised the quality of care at the public hospital and the parents made the decision for the five-year-old to stay put. The next morning, the child’s condition had deteriorated and a doctor from the university teaching hospital was called to attend the private clinic.
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">The child was immediately transferred and underwent life changing surgery to remove almost his entire bowel. The diagnosis; an obstructed superior mesenteric artery, the sole blood supply to the bowel. If the child had been transferred the previous night as suggested, they may have been able to bypass the blocked artery.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="cardnav">
    <div class="container">
        <h4 class="underlined" style="margin-bottom: 5rem;">Other key areas of concern:</h4>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-5">
                <div class="panel">
                    <div class="text-cont trans-slow">
                        <h5>Air Ambulances</h5>
                        <p>It’s not always the answer. We outline the What, When and Why of an Air Ambulance.</p>
                                                <a class="tif-btn red mt-3" href="air-ambulance.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/airlift-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-lg-5">
                <div class="panel" style="margin-top: 2rem;">
                    <div class="text-cont trans-slow">
                        <h5>Premature Babies</h5>
                        <p>Timing is Everything. We discuss our Considerations for Premature Babies Abroad.</p>
                                                <a class="tif-btn red mt-3" href="premature-babies.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/prem-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>

<section class="callout">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">WE ARE NOT THE ONLY COMPANY IN THE INDUSTRY THAT ARE AWARE OF THESE ISSUES, BUT WE BELIEVE WE ARE THE ONLY ONE CURRENTLY PREPARED TO TACKLE THE PROBLEMS.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="jumbo red" style="background-image: url('images/stories-hero.jpg');">
    <div class="shape"></div>
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">your stories</h2>
            <p>We have real case studies that are quite frankly horror stories. We want you to be aware of these situations, so you can at the very least choose to protect yourselves, whether you are insured by us or not.</p>
            <a class="tif-btn black mt-3" href="community.php#stories">Learn more</a><br>
            <a class="tif-btn mt-3" href="mailto:hello@tifgroup.co.uk">Tell us yours</a><br>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<?php include('footer.php'); ?>

</body>

</html>
