<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TIF Group | Air Ambulance</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link href="https://fonts.googleapis.com/css?family=Oswald:500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/master.css">
    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

        })(window,document,'script','dataLayer','GTM-P7RZ5VH');</script>

    <!-- End Google Tag Manager -->
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<?php include('header.php'); ?>

<section class="jumbo red black layr animated fadeIn" style="background-image: url('images/air-hero.jpg');">
    <a class="scroll-arrow trans" href="#more"><img src="images/scroll-arrow.png"></a><!-- end scroll arrow -->
    <div class="background" style="background-image: url('images/air-hero.jpg');"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-1">
            <h1 class="mb-3 animated fadeInDown">Air<br>Ambulances</h1>
            <div class="animated fadeInUp">
                <p>We understand customers want to come home as quickly as possible after falling ill abroad, we really do, and we want to get our customers home – we just aren’t prepared to do that until it is safe to do so.</p>
                <a class="tif-btn red mt-3" href="#more">Learn more</a>
            </div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial align-left ambulance" id="more">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/air1.jpg" class="picture layr" data-rellax-speed="2" style="top:30%;">
        <div class="text-cont layr" data-rellax-speed="1">
            <p>Sometimes, when travelling the unfortunate can happen that results in you needing medical treatment abroad. You may be in unfamiliar territory, sitting in a hospital ward whilst doctors and nurses are communicating in a language unknown, and feeling very vulnerable. The only thing on your mind is to return home whilst being supported by your loved ones and receiving treatment from NHS standard clinics.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="1">
            <p>This is not an uncommon feeling, and rightfully so as during times of vulnerability, we all want to be in familiar and secure surroundings. Whilst you may want to return home as soon as possible,  there are extremely good reasons why returning home immediately after medical treatment abroad may not be in the best interest of your health and safety.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="1">
            <p>These are 5 popular destinations for British Holidaymakers, whose public healthcare systems are ‘better’ than us at medicine based on the World Health Organisation report.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="1">
            <p>When we transport our customers back home, we may decide to use air ambulances. Air Ambulances, are similar to regular ambulances, they are equipped with appropriate but limited apparatus vital to monitoring and treating injured or ill patients. On the other hand, there are very good reasons why we may decide not to use, or delay the use of air ambulance in some situations.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/air2.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">An Air Ambulance is used when the risk of a patient remaining where they are for treatment is greater than the risk associated with them being transferred.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial reverse small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/air3.jpg" class="picture layr" data-rellax-speed="-0.5" style="top:35%;max-width: 400px;">
        <div class="text-cont layr mt-5" data-rellax-speed="1">
            <p>A request for an Air Ambulance is taken very seriously and is reviewed by our medical team, who have to consider what risk is attached to transferring patients by air either commercially or in an Air Ambulance.  Their decision is based upon what is medically appropriate.</p>
        </div>
        <div class="text-cont layr mt-5" data-rellax-speed="3">
            <p>We understand customers want to come home as quickly as possible after falling ill abroad and we want to get our customers home we just aren’t prepared to do so until it is medically safe to do so.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/air4.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-4">Have you seen a water bottle pressurise on a plane?</h2>
            <h2 class="mb-4">Have you felt the pressure in your ears?</h2>
            <h2 class="mb-0">Do you understand what flight, altitude, air pressure and the force associated with take-off and landing do to your body?</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="story large-marge">
    <div class="container-fluid">
        <div class="row no-gutters align-items-center">
            <div class="col-12 col-md-5"><img src="images/air5.jpg" class="picture layr" data-rellax-speed="-0.5"></div><!-- end col -->
            <div class="col-12 col-md-7">
                <div class="text-cont mt-0">
                    <p>Flying at altitude when you fall sick or injured means your health is compromised and your body is fragile and weak.</p>
                </div>
                <div class="text-cont">
                    <p>If someone is unwell, their health is compromised. Their body is fragile, weak, their organs are struggling, they are post- surgery, have excess fluid in places they shouldn’t be, have abnormalities to blood levels, as examples; Then flight, aviation at any level and even in an Air Ambulance, is extremely dangerous and is likely to worsen a condition - in a number of medical situations it can result in premature death.</p>
                </div>
                <div class="text-cont">
                    <p>We refer to this as being ‘medically contraindicated’ - A particular course of action being inadvisable on medical grounds which could result in more harm than good</p>
                </div><!-- end text cont -->
                <div class="text-cont">
                    <p>There is a lot of information available on the effects of aviation and altitude on the body. Our decisions are considered carefully with all the medical information we can obtain from the treating doctor, which is then passed to our medical team and aviation specialists who make an assessment based on their clinical expertise.</p>
                </div><!-- end text cont -->
                <div class="text-cont">
                    <p>We make a conscious decision to consider the effect of the body, organs, bodily functions and clinical stability and recovery of a patient.</p>
                </div><!-- end text cont -->
            </div><!-- end col -->
        </div>
    </div><!-- end container -->
</section>

<section class="callout" style="background-image: url('images/air6.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3 mb-xl-4">Our decision to not use an air ambulance prematurely is a decision based on protecting life.</h2>
            <h2 class="mb-0">We don’t want people to be exposed to unnecessary risk of harm.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial reverse small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/air7.jpg" class="picture layr" data-rellax-speed="-0.5" style="top:35%;max-width: 400px;">
        <div class="text-cont layr mt-5" data-rellax-speed="2">
            <p>We know that when people find themselves in a medical situation overseas, they are frightened and quite simply just want to get home as quickly as possible. When this wish to get home is delayed or refused, we appreciate the frustration this can cause.</p>
        </div>
        <div class="text-cont layr mt-5" data-rellax-speed="2">
            <p>Our doctors know that their decision to either delay or refuse an Air Ambulance is going to come as a disappointment to our customers and their loved ones. It is possible that an Air Ambulance transfer is an option some days or weeks later than requested, but only when the clinical risk of moving a patient is less than the risk of not moving.</p>
        </div>
        <div class="text-cont layr mt-5" data-rellax-speed="2">
            <p>This is our decision and one which they and we will stand by because it is the right thing to do for our customers best interests.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="banner layr" data-rellax-speed="1" style="background-image: url('images/air8.jpg');">
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-2"><h3>Sometimes it's best to stay put</h3></div>
    </div>
</section>

<section class="story large-marge">
    <div class="container-fluid">
        <div class="row no-gutters align-items-center">
            <div class="col-12 col-md-7">
                <div class="text-cont mt-0">
                    <p>We understand and completely appreciate customers may not want to remain in overseas hospital. However, at times it is simply not medically appropriate for customers to return home before they have treatment. tifgroup assess each circumstance on a clinical basis, with knowledge of the treating hospitals; to establish that the required treatment is available and suitable locally.</p>
                </div>
                <div class="text-cont">
                    <p>We make decisions as to whether there is any clinical benefit that outweighs the risk involved in a delay of immediate treatment and undertaking the complications associated with transfer back to the United Kingdom for treatment.</p>
                </div><!-- end text cont -->
            </div><!-- end col -->
            <div class="col-12 col-md-5"><img src="images/air9.jpg" class="picture layr" data-rellax-speed="-0.5"></div><!-- end col -->
        </div>
    </div><!-- end container -->
</section>

<section class="story large-marge">
    <div class="container-fluid">
        <div class="row no-gutters align-items-center">
            <div class="col-12 col-md-7">
                <div class="text-cont mt-0">
                    <p>If customers are worried about the standard of care they may receive, it is useful to understand that the UK health service is actually ranked as inferior to a number of countries both inside and outside of Europe:</p>
                </div>
                <div class="text-cont">
                    <p>These are 5 typical destinations from British Holidaymakers, who are ‘better’ than us at medicine based on the World Health Organisation report.</p>
                </div><!-- end text cont -->
            </div><!-- end col -->
            <div class="col-12 col-md-5 pr-lg-5">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Country</th>
                            <th>Position</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr><td>France</td><td>1</td></tr>
                        <tr><td>Italy</td><td>2</td></tr>
                        <tr><td>Spain</td><td>7</td></tr>
                        <tr><td>Portugal</td><td>12</td></tr>
                        <tr><td>Greece</td><td>14</td></tr>
                        <tr><td>United Kingdom</td><td>18</td></tr>
                    </tbody>
                </table>
            </div><!-- end col -->
        </div>
    </div><!-- end container -->
</section>

<section class="callout" style="background-image: url('images/air11.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">The truth is, that many countries of the world have outstanding medical care and a number are superior to the NHS.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="story large-marge">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7"><div class="text-cont mx-0" style="max-width: none!important;"><h5>There are only usually two reasons why tifgroup would support the transfer of a patient back to the UK over continued treatment in resort:</h5></div></div><!-- end col -->
        </div><!-- end row -->
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">
                <div class="d-flex text-cont">
                    <span class="number">1.</span>
                    <p>There is a lack of adequate medical facilities locally, as confirmed by our medical team.</p>
                </div>
            </div><!-- end col -->
            <div class="col-12 col-md-1 col-lg-2"></div>
        </div><!-- end row -->
        <div class="row justify-content-end">
            <div class="col-12 col-md-5">
                <div class="d-flex text-cont">
                    <span class="number">2.</span>
                    <div>
                        <p>The treatment required is long term and would be best to be undertaken by a usual consultant or a UK NHS based consultant for continuity of care for long term, chronic conditions.</p>
                        <p>If there are adequate facilities available locally, we would recommend the immediate treatment in resort followed by a period of stability, where we then make arrangements to get our customers home in the event, they cannot return on their original planned return date.</p>
                    </div>
                </div>
            </div><!-- end col -->
            <div class="col-12 col-md-1"></div>
        </div><!-- end row -->
    </div>
</section>

<section class="callout" style="background-image: url('images/air7.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">tifgroup have not and will not proceed with arrangements against the advice of our medical team and aviation specialists. Our decision is based upon achieving the very best medical outcome for you.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="story large-marge">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7"><div class="text-cont mx-0" style="max-width: none!important;"><h5>In the event of a conflict of our recommendations; one of two things happen and we always hope it’s the first one:</h5></div></div><!-- end col -->
        </div><!-- end row -->
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">
                <div class="d-flex text-cont">
                    <span class="number">1.</span>
                    <p>Customers are unhappy about the situation and make it clear that they intend to complain, to the highest level. BUT they do take our advice and allow us to repatriate them when it’s safe to do so.</p>
                </div>
            </div><!-- end col -->
            <div class="col-12 col-md-1 col-lg-2"></div>
        </div><!-- end row -->
        <div class="row justify-content-end">
            <div class="col-12 col-md-5">
                <div class="d-flex text-cont">
                    <span class="number">2.</span>
                    <div>
                        <p>We reluctantly provide customers with details of AA companies we know and trust, for our customers to arrange the AA themselves. If the customer or their family, having had the risks explained to them by our doctor, wish to try organise this themselves then they are free to do so and to then seek reimbursement from the insurer.</p>
                        <p>However, accepting that any failed attempts of repatriation are at their own risk. </p>
                    </div>
                </div>
            </div><!-- end col -->
            <div class="col-12 col-md-1"></div>
        </div><!-- end row -->
        <div class="row">
            <div class="col-12 col-md-7 col-xl-6"><div class="text-cont mx-0" style="max-width: none!important;">
                    <p>Our hope in these situations, is that despite being unhappy with the decision to delay or refuse an Air Ambulance, that our customers and their families at the very least listen to our recommendations and allow our medical team to proceed with clinically appropriate arrangements.</p>
                </div></div><!-- end col -->
        </div><!-- end row -->
    </div>
</section>

<section class="editorial small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="1">
            <a class="tif-btn red mt-3" href="air-ambulances.pdf" target="_blank">Read this as a PDF</a>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h4 class="text-left">A <span style="color:red;">true</span> story</h4>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">We had an elderly patient overseas who had suffered a subdural brain haemorrhage; a bleed of the brain caused by trauma or a significant damage to the blood vessels in the brain. The outlook for Subdural Haemorrhages is generally poor and a proportion of people die even with prompt surgery. It usually results in injuries to other parts of the brain and can result in long term neurological problems, as it causes pressure to the brain (intracranial pressure) which can damage very delicate brain tissue, if the patient survives.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">The patient was in an area of the world where the medical facilities are unfortunately inferior to the UK. This is a geographical hazard that we can’t control but instead had to consider whether the risks of him staying there were greater than moving him.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">The patient’s clinical situation meant he was unlikely to be well enough to return home on a normal plane and so it was known that at the point that his condition stabilised and he met the clinical requirements for aviation transfer by Air Ambulance that this would be the course of action taken. The medical information provided to us from the treating Doctor, was passed to our Doctors, and one specifically who has worked at one of the most respected Neurological centres in the United Kingdom, so was best placed to make the assessment as to when to transfer him. The family made it clear that they wanted him back in the UK, immediately and did not appreciate the recommendations made by our Doctor.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">The reality was that this patient had swelling on the Brain and placing him into an Air Ambulance at that time, would have further deteriorated his condition and the risk of fatality was high, something our doctors were not prepared to risk. After much pressure from the family, we provided them with details of air ambulances companies they could use, sadly within 36 hours the patient had passed away. This was not as a result of him having been denied transfer by AA, this was something we had considered at the time, to be a high possibility. We were desperately trying to prevent a fatal outcome and preventing his death included NOT putting his brain into an environment that is contraindicated to his clinical situation and where we were unable to keep tight control of physiological variables that could make what was very critical situation, worse.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">There wasn’t anything we could have done to prevent this terrible outcome. We simply tried to keep their loved one safe and alive for as long as we could by making sure he was not placed into any unnecessary risk.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">Our moral obligation to try and assist the patient and their family unfortunately resulted in a complaint, which at its heart was an accusation above our motives. To that, our motives was simply to ensure best clinical outcome for our customer.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="cardnav">
    <div class="container">
        <h4 class="underlined" style="margin-bottom: 5rem;">Other key areas of concern:</h4>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-5">
                <div class="panel">
                    <div class="text-cont trans-slow">
                        <h5>Private Hospitals</h5>
                        <p>Our position on where customers receive medical treatment is based solely on optimal care.</p>
                                                <a class="tif-btn red mt-3" href="private-hospitals.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/private-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-lg-5">
                <div class="panel" style="margin-top: 2rem;">
                    <div class="text-cont trans-slow">
                        <h5>Premature Babies</h5>
                        <p>Timing is Everything. We discuss our Considerations for Premature Babies Abroad.</p>
                                                <a class="tif-btn red mt-3" href="premature-babies.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/prem-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>

<section class="callout">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">WE ARE NOT THE ONLY COMPANY IN THE INDUSTRY THAT ARE AWARE OF THESE ISSUES, BUT WE BELIEVE WE ARE THE ONLY ONE CURRENTLY PREPARED TO TACKLE THE PROBLEMS.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="jumbo red" style="background-image: url('images/stories-hero.jpg');">
    <div class="shape"></div>
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">your stories</h2>
            <p>We have real case studies that are quite frankly horror stories. We want you to be aware of these situations, so you can at the very least choose to protect yourselves, whether you are insured by us or not.</p>
            <a class="tif-btn black mt-3" href="community.php#stories">Learn more</a><br>
            <a class="tif-btn mt-3" href="mailto:hello@tifgroup.co.uk">Tell us yours</a><br>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<?php include('footer.php'); ?>

</body>

</html>
