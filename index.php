<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TIF Group</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link href="https://fonts.googleapis.com/css?family=Oswald:500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/master.css">
    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

        })(window,document,'script','dataLayer','GTM-P7RZ5VH');</script>

    <!-- End Google Tag Manager -->
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<?php include('header.php'); ?>

<section class="jumbo" style="background-image: url('images/homepage-hero.jpg');">
    <a class="scroll-arrow trans" href="#more">
        <img src="images/scroll-arrow.png">
    </a><!-- end scroll arrow -->
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="0.5">
            <h1 class="mb-3 animated fadeInDown slow">Our Philosophies</h1>
            <div class="animated fadeInUp slower">
                <p>From humble beginnings to now being the largest travel insurer in the UK, our position is to do what is in the best interest of our customers.</p>
                <p>As we’ve grown, we have seen an increasing number of complexities and challenges for holidaymakers overseas.</p>
                <p>This is why we’re introducing our philosophies – a transparent playbook into our practices, highlighting our moral obligation to you, the UK Holidaymaker.</p>
                <a class="tif-btn red mt-3" href="#more">Learn more</a>
            </div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="callout" id="more">
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="1">
            <h2 class="mb-3">WE BELIEVE IN DOING THE RIGHT THING – WHILST WE ARE A BUSINESS, WE ARE ALSO HUMAN. WE HAVE WITNESSED FIRST-HAND THE IMMORAL AND UNETHICAL PRACTICES AROUND THE WORLD AND OUR MISSION IS SIMPLE:</h2>
            <h2 class="m-0">We are committed to do what is right by you.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="chapter_nav">
    <div class="container-fluid">
        <ul class="nav">
            <li class="trans-slow">
                <div class="text-cont trans-deadslow">
                    <h4 class="mb-3 layr" data-rellax-speed="1.5">Our Ethics</h4>
                    <p class="layr" data-rellax-speed="1">Our moral obligation to you.</p>
                    <a class="tif-btn red mt-3 layr" data-rellax-speed="0.5" href="ethics.php">Learn more</a>
                </div><!-- end text cont -->
            </li>
            <li class="trans-slow">
                <div class="text-cont trans-deadslow">
                    <h4 class="mb-3 layr" data-rellax-speed="1.5">Our Stance</h4>
                    <p class="layr" data-rellax-speed="1">Highlighting what goes on, what we know and what we are trying to do.</p>
                    <a class="tif-btn red mt-3 layr" data-rellax-speed="0.5" href="stance.php">Learn more</a>
                </div><!-- end text cont -->
            </li>
            <li class="trans-slow">
                <div class="text-cont trans-deadslow">
                    <h4 class="mb-3 layr" data-rellax-speed="1.5">Our Community</h4>
                    <p class="layr" data-rellax-speed="1">Introducing those at the heart of tifgroup.</p>
                    <a class="tif-btn red mt-3 layr" data-rellax-speed="0.5" href="community.php">Learn more</a>
                </div><!-- end text cont -->
            </li>
        </ul>
    </div><!-- end container -->
</section>

<!--<section class="message">-->
<!--    <div class="container">-->
<!--        <div class="text-cont">-->
<!--            <h3 class="mb-4 mb-md-5 layr" data-rellax-speed="0.75">Message from our director</h3>-->
<!--            <div class="paragraph mb-md-4 layr" data-rellax-speed="0.75">-->
<!--                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias animi culpa debitis hic illum, itaque iure laboriosam sint veniam. At delectus deleniti eaque fugit natus omnis placeat soluta suscipit ullam?</p>-->
<!--            </div><!-- end text -->
<!--            <div class="clearfix"></div>-->
<!--            <span class="source layr" data-rellax-speed=".75">-->
<!--                <span>Bob Barkely, 32</span><br>-->
<!--                <span>City of London</span>-->
<!--            </span>-->
<!--        </div><!-- end text cont -->
<!--    </div><!-- end container -->
<!--</section>-->

<section class="callout">
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="1">
            <h2 class="mb-0">WE ARE AND WILL CONTINUE TO TRY AND PROTECT OUR CUSTOMERS FROM POOR PRACTICE AND UNNECESSARY RISK, WHILST TRYING TO ENSURE OPTIMAL CARE.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="jumbo red" style="background-image: url('images/stories-hero.jpg');">
    <div class="shape"></div>
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">your stories</h2>
            <p>We have real case studies that are quite frankly horror stories. We want you to be aware of these situations, so you can at the very least choose to protect yourselves, whether you are insured by us or not.</p>
            <a class="tif-btn black mt-3" href="community.php#stories">Learn more</a><br>
            <a class="tif-btn mt-3" href="mailto:hello@tifgroup.co.uk">Tell us yours</a><br>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<?php include('footer.php'); ?>

</body>

</html>
