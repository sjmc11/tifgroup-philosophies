<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TIF Group | Our Community</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link href="https://fonts.googleapis.com/css?family=Oswald:500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/master.css">
    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

        })(window,document,'script','dataLayer','GTM-P7RZ5VH');</script>

    <!-- End Google Tag Manager -->
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<?php include('header.php'); ?>

<section class="jumbo red black layr animated fadeIn">
    <a class="scroll-arrow trans" href="#more"><img src="images/scroll-arrow.png"></a><!-- end scroll arrow -->
    <div class="background" style="background-image: url('images/community-hero.jpg');"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-1">
            <h1 class="mb-3 animated fadeInDown">Our Community</h1>
            <div class="animated fadeInUp">
                <p>We are passionate and determined that we will make a stand to change things, regardless of the consequences.</p>
                <p>From our passionate team, to dedicated partners all the way through to our consumers - we want to make a difference for the better good.</p>
                <a class="tif-btn red mt-3" href="#more">Learn more</a>
            </div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="message quote" id="more" style="background-color:#0d0404;">
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-2">
            <div class="paragraph mb-md-4">
                <h4>The purpose of life is not to be happy. It is to be useful, to be honourable, to be compassionate, to have it make some difference that you have lived and lived well.</h4>
                <h5 class="source">- Ralph Waldo Emerson</h5>
            </div><!-- end text -->
            <div class="clearfix"></div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="intro">
    <div class="container">
        <div class="text-cont">
            <p>With good reason, we take a moral stance to do what’s right by our consumers who play a pivotal role in our community. We have real life case studies that have influenced the decision that we make on a day to day basis</p>
            <h4 class="underlined">True stories from members of our community</h4>
        </div>
    </div>
</section>

<section class="story">
    <div class="container-fluid" id="stories">
        <div class="row no-gutters">
            <div class="col-12 col-md-5"><img src="images/story1.jpg" class="picture layr" data-rellax-speed="-0.5"></div><!-- end col -->
            <div class="col-12 col-md-7">
                <div class="text-cont mt-5">
                    <p>We had an elderly patient overseas who had suffered a subdural brain haemorrhage; a bleed of the brain caused by trauma or a significant damage to the blood vessels in the brain.</p>
                    <button class="tif-btn red mt-3" data-toggle="collapse" href="#story1" role="button" aria-expanded="false" aria-controls="story1">Learn more</button>
                </div><!-- end text cont -->
                <div class="sub collapse" id="story1">
                    <div class="text-cont">
                        <p>The outlook for Subdural Haemorrhages is generally poor and a proportion of people die even with
                            prompt surgery. It usually results in injuries to other parts of the brain and can result in long term
                            neurological problems, as it causes pressure to the brain (intracranial pressure) which can damage
                            very delicate brain tissue, if the patient survives.</p><p>The patient was in an area of the world where the medical facilities are unfortunately inferior to the
                            UK. This is a geographical hazard that we can’t control but instead had to consider whether the risks
                            of him staying there were greater than moving him.</p><p>The patient’s clinical situation meant he was unlikely to be well enough to return home on a normal
                            plane and so it was known that at the point that his condition stabilised and he met the clinical
                            requirements for aviation transfer by Air Ambulance that this would be the course of action taken.
                            The medical information provided to us from the treating Doctor, was passed to our Doctors, and
                            one specifically who has worked at one of the most respected Neurological centres in the United
                            Kingdom, so was best placed to make the assessment as to when to transfer him.</p><p>
                            The family made it clear that they wanted him back in the UK, immediately and did not appreciate
                            the recommendations made by our Doctor.</p><p>
                            The reality was that this patient had swelling on the Brain and placing him into an Air Ambulance at
                            that time, would have further deteriorated his condition and the risk of fatality was high, something
                            our doctors were not prepared to risk. After much pressure from the family, we provided them with
                            details of air ambulances companies they could use, sadly within 36 hours the patient had passed
                            away. This was not as a result of him having been denied transfer by AA, this was something we had
                            considered at the time, to be a high possibility.</p><p>We were desperately trying to prevent a fatal
                            outcome and preventing his death included NOT putting his brain into an environment that is
                            contraindicated to his clinical situation and where we were unable to keep tight control of
                            physiological variables that could make what was very critical situation, worse.
                            There wasn’t anything we could have done to prevent this terrible outcome. We simply tried to keep
                            their loved one safe and alive for as long as we could by making sure he was not placed into any
                            unnecessary risk.</p><p>Our moral obligation to try and assist the patient and their family unfortunately resulted in a
                            complaint , which at its heart was an accusation above our motives. To that, our motives was simply
                            to ensure best clinical outcome for our customer.</p>
                    </div>
                </div>
            </div><!-- end col -->
        </div>
    </div><!-- end container -->
</section>

<section class="story">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-12 col-md-7">
                <div class="text-cont mt-5">
                    <p>A female traveller in her twenties was backpacking in Thailand and suffered an accident resulting in a spinal injury. She was immediately taken to a private clinic.</p>
                    <button class="tif-btn red mt-3" data-toggle="collapse" href="#story2" role="button" aria-expanded="false" aria-controls="story2">Learn more</button>
                </div><!-- end text cont -->
                <div class="sub collapse" id="story2">
                    <div class="text-cont">
                        <p>The distance between the private clinic and the state-run facility was equal.
                            She was admitted on a stretcher and her credit card taken from her bag, a card machine
                            handed to her in the reception area, whilst she was laid flat on a stretcher, to pay a deposit
                            of £500 immediately and in advance of her being treated. At the same time the clinic took
                            her passport.</p><p>
                            She remained in hospital for 4 days, receiving pain relief but with no formal examination
                            from an Orthopaedic surgeon. She was back braced and laid flat, there were no Orthopaedic
                            beds and so she was propped up occasionally using pillows.</p><p>
                            Upon calling tifgroup we advised that she needed to attend the University Teaching
                            Hospital, to have both X-Rays and MRI scans, to establish the full extent of her injuries. We
                            contacted the private clinic who refused to speak to us, instead they lied to the patient and
                            advised her that her insurers were not covering the costs and demanded payment in full
                            from the patient of £6,500. The patient refused, explaining that £6,500 seemed like a lot of
                            money in Thailand for 4 days stay, pain relief and a back brace.</p><p>The hospital staff became
                            immediately hostile, they advised that she would not be able to leave the country as they
                            had her passport and threatened her with police action if she did not pay.
                            The patient still refused to hand over her credit card having already paid £500 deposit. She
                            then called tifgroup again, who then in turn called the consulate to assist as the clinic
                            continued to refuse to deal with our teams. The consulate visited the facility, was unable to
                            see or access the patient and was threatened by hospital staff with death if he ever visited
                            their clinic again.</p><p>
                            Despite many attempts made by tifgroup to deal directly with the private hospital they
                            continued to refuse to deal with an overseas insurer, the hospital staff then subsequently
                            withdrew her pain relief, leaving her in agony, and with no choice she had to pay the
                            amount across her credit card, debit card and through a phone call with her parents to pay
                            the money. She was then ‘released’ with no discharge report and was able to make her way
                            to the university teaching hospital who diagnosed her with a single fracture to a vertebra
                            that could be conservatively treated, but a prolapsed disc exacerbated by a delay in
                            appropriate treatment and support.</p>
                    </div>
                </div>
            </div><!-- end col -->
            <div class="col-12 col-md-5"><img src="images/story3.jpg" class="picture layr" data-rellax-speed="-0.5"></div><!-- end col -->
        </div>
    </div><!-- end container -->
</section>

<section class="story">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-12 col-md-5"><img src="images/story2.jpg" class="picture layr" data-rellax-speed="-0.5"></div><!-- end col -->
            <div class="col-12 col-md-7">
                <div class="text-cont mt-5">
                    <p>A five-year-old was taken to a private hospital in a popular Spanish resort after complaining of stomach pain.</p>
                    <button class="tif-btn red mt-3" data-toggle="collapse" href="#story3" role="button" aria-expanded="false" aria-controls="story3">Learn more</button>
                </div><!-- end text cont -->
                <div class="sub collapse" id="story3">
                    <div class="text-cont">
                        <p>The private hospital diagnosed gastroenteritis placed onto an IV saline
                            solution for rehydration and declared the child would be fine in a few days. tifgroup, advised
                            that the child should be transferred to a children’s unit attached to a university teaching
                            hospital but the private clinic criticised the quality of care at the public hospital and the
                            parents made the decision for the five-year-old to stay put. The next morning, the child’s
                            condition had deteriorated and a doctor from the university teaching hospital was called to
                            attend the private clinic.</p><p>The child was immediately transferred and underwent life changing surgery to remove
                            almost his entire bowel.</p><p>The diagnosis; an obstructed superior mesenteric artery, the sole blood supply to the
                            bowel. If the child had been transferred the previous night as suggested, they may have
                            been able to bypass the blocked artery.</p>
                    </div>
                </div>
            </div><!-- end col -->
        </div>
    </div><!-- end container -->
</section>

<section class="cardnav">
    <div class="container">
        <h4 class="underlined" style="margin-bottom: 7rem;">We have outlined Our Stance into key areas of concern, which are as follows:</h4>
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="panel">
                    <div class="text-cont trans-slow">
                        <h5>Private Hospitals</h5>
                        <p>Corrupt practices; obscene overtreatment, risk of clinical harm and unlawful activity. We highlight our experiences and the action we are taking.</p>
                        <a class="tif-btn red mt-3" href="private-hospitals.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/private-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="panel" style="margin-top: 3rem;">
                    <div class="text-cont trans-slow">
                        <h5>Air Ambulances</h5>
                        <p>It’s not always the answer. We outline the What, When and Why of an Air Ambulance.</p>
                        <a class="tif-btn red mt-3" href="air-ambulance.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/airlift-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-lg-4">
                <div class="panel" style="margin-top: 6rem;">
                    <div class="text-cont trans-slow">
                        <h5>Premature Babies</h5>
                        <p>Timing is Everything. We discuss our Considerations for Premature Babies Abroad.</p>
                        <a class="tif-btn red mt-3" href="premature-babies.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/prem-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>

<section class="callout" style="background-image: url('images/story4.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">OUR VALUES COME DIRECTLY FROM OUR TEAM, WE SEE THEM MORE THAN JUST WORDS, THEY UNDERPIN THE WAY WE WORK AND REFLECT OUR UNDERLYING BELIEFS, ATTITUDE AND BEHAVIOUR.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="jumbo red" style="background-image: url('images/stories-hero.jpg');">
    <div class="shape"></div>
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">your stories</h2>
            <p>We have real case studies that are quite frankly horror stories. We want you to be aware of these situations, so you can at the very least choose to protect yourselves, whether you are insured by us or not.</p>
            <a class="tif-btn black mt-3" href="#stories">Read more</a><br>
            <a class="tif-btn mt-3" href="mailto:hello@tifgroup.co.uk">Tell us yours</a><br>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<?php include('footer.php'); ?>

</body>

</html>
