<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TIF Group | Air Ambulance</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link href="https://fonts.googleapis.com/css?family=Oswald:500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/master.css">
    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

        })(window,document,'script','dataLayer','GTM-P7RZ5VH');</script>

    <!-- End Google Tag Manager -->
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<?php include('header.php'); ?>

<section class="jumbo red black layr animated fadeIn" style="background-image: url('images/air-hero.jpg');">
    <div class="background" style="background-image: url('images/air-hero.jpg');"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-1">
            <h1 class="mb-3 animated fadeInDown">Air Ambulance</h1>
            <div class="animated fadeInUp">
                <p>We understand customers want to come home as quickly as possible after falling ill abroad, we really do, and we want to get our customers home – we just aren’t prepared to do that until it is safe to do so.</p>
                <a class="tif-btn red mt-3" href="#more">Learn more</a>
            </div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial align-left ambulance" id="more">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/air1.jpg" class="picture layr" data-rellax-speed="2">
        <div class="text-cont layr" data-rellax-speed="1">
            <p>An Air Ambulance is either a helicopter or airplane both referred to as ‘aeromedical’ we refer to them here as ‘AA’s’. They are used when the risk of a patient remaining where they are for treatment is greater than the risk associated with them being transferred.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h3>AIR AMBULANCES CAN BE FATAL, IF NOT TIMED CAREFULLY.</h3>
        </div>
        <div class="text-cont layr" data-rellax-speed="3">
            <p>When it comes to Repatriation there are so many variables and each individual case is considered carefully.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p>Altitude puts the body under extreme stress and has a significant effect on our vitals, organs and bodily functions.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="3">
            <p>If someone is sick, their health is compromised. Their body is fragile, weak, their organs are struggling and at flight altitude it is very dangerous and is likely to worsen a condition - in a number of medical situations it can result in premature death.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/air2.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">HAVE YOU SEEN A WATER BOTTLE PRESSURISE ON A PLANE?</h2>
            <h2 class="mb-4 mb-lg-5">Or felt the pressure in your ears?</h2>
            <h2 class="m-0">Imagine what happens to your body.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial reverse">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/air3.jpg" class="picture layr" data-rellax-speed="-0.5" style="top:35%;max-width: 400px;">
        <div class="text-cont layr mt-5" data-rellax-speed="1">
            <p>With all the medical information we can get from the treating doctor, our medical team and aviation specialists will make an assessment based on their clinical expertise and experience of moving patients across the globe. Our doctors make repatriation decisions in order to protect customers’ health.</p>
        </div>
        <div class="text-cont layr mt-5" data-rellax-speed="0.4">
            <h4 class="text-left pl-md-3 pl-xl-5">WE DON’T WANT PEOPLE TO BE EXPOSED TO UNNECESSARY RISK OF HARM.</h4>
        </div>
        <div class="text-cont layr mt-5" data-rellax-speed="3">
            <p>Sounds unlikely, but it’s what we do and have been doing for over twenty years. We operate differently, with very good reason and that is the best possible chance of good clinical outcome for the customer.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/air4.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">OUR DOCTOR’S DECISION IS ONE WHICH THEY AND WE WILL STAND BY BECAUSE IT IS THE RIGHT THING TO DO FOR OUR CUSTOMERS BEST INTERESTS.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="story large-marge">
    <div class="container-fluid">
        <div class="row no-gutters align-items-center">
            <div class="col-12 col-md-5"><img src="images/air5.jpg" class="picture layr" data-rellax-speed="-0.5"></div><!-- end col -->
            <div class="col-12 col-md-7">
                <div class="text-cont mt-0">
                    <p>We know that when people find themselves in a medical situation overseas, they are frightened and quite simply just want to get home as quickly as possible. When this wish to get home is delayed or refused, we appreciate the upset, we understand that customers become angry, scared, perhaps confused and conflicted.</p>
                </div>
                <div class="text-cont">
                    <p>This can sometimes have us, the insurer, painted as the villain in the process.</p>
                </div>
                <div class="text-cont">
                    <h4>HOWEVER, OUR MOTIVES IN THESE SITUATIONS ARE SIMPLY TO ENSURE BEST CLINICAL OUTCOME WHERE WE ARE ABLE TO CONTROL THE OF EFFECT IT.</h4>
                </div><!-- end text cont -->
            </div><!-- end col -->
        </div>
    </div><!-- end container -->
</section>

<section class="story large-marge">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7"><div class="text-cont mx-0" style="max-width: none!important;"><h5>IN THE EVENT OF A CONFLICT OF OUR RECOMMENDATIONS; ONE OF TWO THINGS HAPPEN:</h5></div></div><!-- end col -->
        </div><!-- end row -->
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">
                <div class="d-flex text-cont">
                    <span class="number">1.</span>
                    <p>Customers are unhappy about the situation and make it clear that they intend to complain, to the highest level. BUT they do take our advice and allow us to repatriate them when it’s safe to do so.</p>
                </div>
            </div><!-- end col -->
            <div class="col-12 col-md-1 col-lg-2"></div>
        </div><!-- end row -->
        <div class="row justify-content-end">
            <div class="col-12 col-md-5">
                <div class="d-flex text-cont">
                    <span class="number">2.</span>
                    <div>
                        <p>We reluctantly provide customers with details of AA companies we know and trust, for our customers to arrange the AA themselves. If the customer or their family, having had the risks explained to them by our doctor, wish to try organise this themselves then they are free to do so and to then seek reimbursement from the insurer. However, accepting that any failed attempts of repatriation are at their own risk.</p>
                        <p>Our hope in these situations, is that despite being unhappy with the decision to delay or refuse an AA, that our customers and their families at the very least listen to our recommendations and allow our medical team to proceed with clinically appropriate arrangements.</p>
                    </div>
                </div>
            </div><!-- end col -->
            <div class="col-12 col-md-1"></div>
        </div><!-- end row -->
    </div>
</section>

<section class="callout" style="background-image: url('images/air6.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">CONVENIENCE SHOULDN’T TAKE PRIORITY OVER BEST MEDICAL OUTCOME.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="cardnav">
    <div class="container">
        <h4 class="underlined" style="margin-bottom: 5rem;">Other key areas of concern:</h4>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-5">
                <div class="panel">
                    <div class="text-cont trans-slow">
                        <h5>Private Hospitals</h5>
                        <p>Our position on where customers receive medical treatment is based solely on optimal care.</p>
                        <a class="tif-btn red mt-3" href="private-hospitals.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/private-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-lg-5">
                <div class="panel" style="margin-top: 2rem;">
                    <div class="text-cont trans-slow">
                        <h5>Premature Babies</h5>
                        <p>Timing is Everything. We discuss our Considerations for Premature Babies Abroad.</p>
                        <a class="tif-btn red mt-3" href="premature-babies.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/prem-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>

<section class="callout">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">WE ARE NOT THE ONLY COMPANY IN THE INDUSTRY THAT ARE AWARE OF THESE ISSUES, BUT WE BELIEVE WE ARE THE ONLY ONE CURRENTLY PREPARED TO TACKLE THE PROBLEMS.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="jumbo red" style="background-image: url('images/stories-hero.jpg');">
    <div class="shape"></div>
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">your stories</h2>
            <p>We have real case studies that are quite frankly horror stories. We want you to be aware of these situations, so you can at the very least choose to protect yourselves, whether you are insured by us or not.</p>
            <a class="tif-btn black mt-3" href="community.php#stories">Learn more</a><br>
            <a class="tif-btn mt-3" href="mailto:hello@tifgroup.co.uk">Tell us yours</a>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<?php include('footer.php'); ?>

</body>

</html>
