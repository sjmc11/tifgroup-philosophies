<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TIF Group | Private Hospitals</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link href="https://fonts.googleapis.com/css?family=Oswald:500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/master.css">
    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

        })(window,document,'script','dataLayer','GTM-P7RZ5VH');</script>

    <!-- End Google Tag Manager -->
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<?php include('header.php'); ?>

<section class="jumbo red black layr animated fadeIn">
    <div class="background" style="background-image: url('images/private-hero.jpg');"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-1">
            <h1 class="mb-3 animated fadeInDown">Private<br>Hospitals</h1>
            <div class="animated fadeInUp">
                <p>Our position on where customers receive medical treatment is based solely on optimal care.</p>
                <a class="tif-btn red mt-3" href="#more">Learn more</a>
            </div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial reverse align-right ambulance" id="more">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/private1.jpg" class="picture layr" data-rellax-speed="2" style="top: 15rem;">
        <div class="text-cont layr" data-rellax-speed="1">
            <p>Both private and public facilities in the United Kingdom, are regulated in the same way by the same entity.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h3>THERE IS NO SUCH REGULATION IN PLACE IN SOME PRIVATE CLINICS ACROSS THE WORLD.</h3>
        </div>
        <div class="text-cont layr" data-rellax-speed="3">
            <p>We have had a known case where a doctor qualified to GP level was prepared to undertake orthopaedic surgery with no training to do so.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="3">
            <p>The reality associated with this, is that patients at a private facility overseas could have serious medical situations overlooked or misdiagnosed due to lack of appropriate diagnostics or expertise available. We have cases that prove this and cases where unfortunately the clinical outcome has been life changing.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/private2.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">TRAVEL INSURANCE IS NOT PRIVATE HEALTH INSURANCE.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial reverse small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/private3.jpg" class="picture layr" data-rellax-speed="-0.5" style="top:15%;max-width: 400px;">
        <img src="images/private4.jpg" class="picture layr" data-rellax-speed="-0.5" style="top: 40%;right:3rem;">
        <div class="text-cont layr mt-2" data-rellax-speed="1.5">
            <p>For us, access to the best available care for our customers in a medical situation has and will always come before convenience or cost.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2">
            <p class="text-left pl-md-3 pl-xl-5">The convenience of a medical facility close to the customer’s hotel or the comfort of a private room with British satellite TV, menu choices and shiny marble floors, cannot be the criteria for choosing where to seek emergency treatment; quality of clinical care must override all other considerations.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2.5">
            <p>SA private facility in a tourist resort may not have the expertise in all areas of medicine, it is possible that some practitioners are NOT fully qualified or have NOT completed up to date training.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/private5.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">CHOOSING STYLE OVER SUBSTANCE IN A MEDICAL SITUATION CAN HAVE SIGNIFICANT AND SEVERE CONSEQUENCES TO YOUR HEALTH.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial stance small-gap align-left">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/private6.jpg" class="picture layr" data-rellax-speed="2" style="top: 25%;right: 4rem;">
        <div class="text-cont layr" data-rellax-speed="1">
            <p>Some private clinics have been found to have set up commercial contracts with third parties, such as hotels, which advise holidaymakers to go to those clinics rather than to more appropriate and better equipped public hospitals for emergency treatment.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p>An automatic referral of a patient to a private clinic in this way, without an assessment as to the best facility for the treatment of the patient,</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h4 class="pl-5 text-left">REMOVES PATIENT CHOICE.</h4>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/private7.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">WE HAVE HAD SIGHT OF A ‘COLLABORATION AGREEMENT’ BETWEEN A LARGE NUMBER OF MEDICAL CLINICS AND A LARGE CHAIN OF HOTELS BASED WHICH PROVIDES FOR THE EXCLUSIVE TRANSFER OF TOURISTS TO CERTAIN NAMED CLINICS UNDER THE AGREEMENT.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="1">
            <p class="pl-0">Private hospitals overseas are renowned for over-treating, over-examining and over-charging holidaymakers -</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h4 class="text-left">THEY ARE FINANCIALLY MOTIVATED BY YOUR TREATMENT.</h4>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">A clear indication as to whether a facility has a financial incentive over your care and admission is often demonstrated as soon as you step in the door. Some private hospitals will demand an upfront payment or a credit card/passport before even considering a medical assessment – regardless of how injured or ill you are when you arrive.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">We have had members of nursing staff withdraw much needed pain relief from a patient in order to extract money from them.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h4 class="text-left">THIS IS ILLEGAL, IMMORAL AND AN ABSOLUTE BREACH OF DUTY OF CARE.</h4>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">So, when your insurer advises you to transfer to a public hospital is not travel insurers trying to get out of paying your claim, it is simply to ensure you receive the best possible care and to mitigate the risk of clinical or emotional harm.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/private8.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">WE ARE CHALLENGING THE PRACTICE OF THESE ‘TOURIST TRAPS’ WILL CONTINUE TO PUT PATIENTS AT RISK FOR FINANCIAL GAIN.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial reverse align-right small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/private4.jpg" class="picture layr" data-rellax-speed="-0.5" style="top:15%;max-width: 600px;">
        <div class="text-cont layr mt-2" data-rellax-speed="1.5">
            <p class="pl-0">Our philosophy and how we manage cases is part of the action we take daily. We are and will continue to challenging the practice of private clinics that put patients at risk for financial gain.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2">
            <p class="text-left pl-0">We are funding an investigation into the corrupt practices of hoteliers and private medical facilities, whereby patients are transferred by medically unqualified hotel staff to private medical facilities pursuant to contractual arrangements which reward hoteliers for their ‘referrals’.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2.5">
            <h4 class="text-left">WE ARE QUITE SIMPLY SPENDING MONEY WITH A VIEW TO PURSUE CRIMINAL PROCEEDINGS IN ORDER TO PROTECT THE INTERESTS OF YOU, THE UK HOLIDAYMAKER.</h4>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="cardnav">
    <div class="container">
        <h4 class="underlined" style="margin-bottom: 5rem;">Other key areas of concern:</h4>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-5">
                <div class="panel">
                    <div class="text-cont trans-slow">
                        <h5>Air Ambulances</h5>
                        <p>It’s not always the answer. We outline the What, When and Why of an Air Ambulance.</p>
                        <a class="tif-btn red mt-3" href="air-ambulance.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/airlift-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-lg-5">
                <div class="panel" style="margin-top: 2rem;">
                    <div class="text-cont trans-slow">
                        <h5>Premature Babies</h5>
                        <p>Timing is Everything. We discuss our Considerations for Premature Babies Abroad.</p>
                        <a class="tif-btn red mt-3" href="premature-babies.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/prem-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>

<section class="callout">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">WE ARE NOT THE ONLY COMPANY IN THE INDUSTRY THAT ARE AWARE OF THESE ISSUES, BUT WE BELIEVE WE ARE THE ONLY ONE CURRENTLY PREPARED TO TACKLE THE PROBLEMS.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="jumbo red" style="background-image: url('images/stories-hero.jpg');">
    <div class="shape"></div>
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">your stories</h2>
            <p>We have real case studies that are quite frankly horror stories. We want you to be aware of these situations, so you can at the very least choose to protect yourselves, whether you are insured by us or not.</p>
            <a class="tif-btn black mt-3" href="community.php#stories">Learn more</a><br>
            <a class="tif-btn mt-3" href="mailto:hello@tifgroup.co.uk">Tell us yours</a>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<?php include('footer.php'); ?>

</body>

</html>
