<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TIF Group | Our Stance</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link href="https://fonts.googleapis.com/css?family=Oswald:500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/master.css">
    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

        })(window,document,'script','dataLayer','GTM-P7RZ5VH');</script>

    <!-- End Google Tag Manager -->
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<?php include('header.php'); ?>

<section class="jumbo red black layr animated fadeIn">
    <a class="scroll-arrow trans" href="#more"><img src="images/scroll-arrow.png"></a><!-- end scroll arrow -->
    <div class="background" style="background-image: url('images/stance-hero.jpg');"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-1">
            <h1 class="mb-3 animated fadeInDown">Our Stance</h1>
            <div class="animated fadeInUp">
                <p>At our very core, we are passionate about our customers and about our industry. We feel so passionately, that not only are we trying to tackle the problem single handed.</p>
                <p>We are raising awareness of the issues so that people can make better decisions and understand the complex world that they may face should they be unlucky enough to fall ill abroad.</p>
                <a class="tif-btn red mt-3" href="#more">Learn more</a>
            </div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<!--<section class="editorial stance">-->
<!--    <div class="clearfix"></div>-->
<!--    <div class="container">-->
<!--        <img src="images/stance2.jpg" class="picture layr" data-rellax-speed="1.5" style="max-width: 490px;">-->
<!--        <img src="images/stance1.jpg" class="picture layr" data-rellax-speed="-2" style="margin-top: -9rem!important;">-->
<!--        <img src="images/stance3.jpg" class="picture layr" data-rellax-speed="0">-->
<!--        <img src="images/stance4.jpg" class="picture layr" data-rellax-speed="1" style="max-width: 740px;bottom:-4rem;">-->
<!--        <div class="text-cont layr mt-4" data-rellax-speed="1">-->
<!--            <p>At our very core, we are passionate about our customers and about our industry. We feel so passionately, that not only are we trying to tackle the problem single handed.</p>-->
<!--        </div>-->
<!--        <div class="text-cont layr" data-rellax-speed="3">-->
<!--            <p>At our very core, we are passionate about our customers and about our industry. We feel so passionately, that not only are we trying to tackle the problem single handed.</p>-->
<!--        </div>-->
<!--        <div class="text-cont layr right mt-5" data-rellax-speed="0.4">-->
<!--            <h3>OUR DUTY OF CARE TO THE UK HOLIDAYMAKER IS TO COMBAT: CORRUPTION, UNETHICAL PRACTICES AND PROTECT LIFE.</h3>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="clearfix"></div>-->
<!--</section>-->

<section class="cardnav" id="more">
    <div class="container">
        <h4 class="underlined" style="margin-bottom: 5rem;">We have outlined Our Stance into key areas of concern, which are as follows:</h4>
            <div class="row">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="panel layr" data-rellax-speed="3">
                    <div class="text-cont trans-slow">
                        <h5>Private Hospitals</h5>
                        <p>Corrupt practices; obscene overtreatment, risk of clinical harm and unlawful activity. We highlight our experiences and the action we are taking.</p>
                        <a class="tif-btn red mt-3" href="private-hospitals.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/private-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="panel layr" style="margin-top: 3rem;" data-rellax-speed="5">
                    <div class="text-cont trans-slow">
                        <h5>Air Ambulances</h5>
                        <p>It’s not always the answer. We outline the What, When and Why of an Air Ambulance.</p>
                        <a class="tif-btn red mt-3" href="air-ambulance.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/airlift-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-lg-4">
                <div class="panel layr" style="margin-top: 6rem;" data-rellax-speed="9">
                    <div class="text-cont trans-slow">
                        <h5>Premature Babies</h5>
                        <p>Timing is Everything. We discuss our Considerations for Premature Babies Abroad.</p>
                        <a class="tif-btn red mt-3" href="premature-babies.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/prem-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>

<section class="callout">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">WE ARE TRULY CHAMPIONING THE UK HOLIDAYMAKER</h2>
            <h2 class="m-0">We are passionate and determined to make a stand for change. The decisions we make are motivated only by best outcome for our customers and not financial concerns.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="jumbo red" style="background-image: url('images/stories-hero.jpg');">
    <div class="shape"></div>
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">your stories</h2>
            <p>We have real case studies that are quite frankly horror stories. We want you to be aware of these situations, so you can at the very least choose to protect yourselves, whether you are insured by us or not.</p>
            <a class="tif-btn black mt-3" href="community.php#stories">Learn more</a><br>
            <a class="tif-btn mt-3" href="mailto:hello@tifgroup.co.uk">Tell us yours</a><br>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<?php include('footer.php'); ?>

</body>

</html>
