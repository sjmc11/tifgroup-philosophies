<div class="menu-wrapper" style="display: none;">
    <nav>
        <ul>
            <li><a href="ethics.php">Our Ethics</a></li>
            <li><a href="stance.php">Our Stance</a></li>
            <li><a href="community.php">Our Community</a></li>
        </ul>
    </nav>
    <div class="stripe first"></div>
    <div class="stripe second"></div>
    <div class="stripe third"></div>
</div><!-- end menu -->

<header class="primary">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-between">
            <div class="col-auto">
                <a class="branding d-inline-block" href="/"><img src="images/logo.svg" width="45" height="auto"></a>
            </div><!-- end col -->
            <div class="col-auto">
                <a class="menu-toggle d-inline-block" id="toggleMenu">
                    <img src="images/close.svg" class="close" width=22" height="auto">
                    <img src="images/hamburger.svg" class="open" width=28" height="auto">
                </a>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</header>