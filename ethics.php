<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TIF Group | Our Ethics</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link href="https://fonts.googleapis.com/css?family=Oswald:500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/master.css">
    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

        })(window,document,'script','dataLayer','GTM-P7RZ5VH');</script>

    <!-- End Google Tag Manager -->
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<?php include('header.php'); ?>

<section class="jumbo red black layr animated fadeIn">
    <a class="scroll-arrow trans" href="#more"><img src="images/scroll-arrow.png"></a><!-- end scroll arrow -->
    <div class="background" style="background-image: url('images/ethics-hero.jpg');"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-1">
            <h1 class="mb-3 animated fadeInDown">Our Ethics</h1>
            <div class="animated fadeInUp">
                <p>There are moral and ethical challenges insurers face, which are not limited to just financial impact to our customers, but to our customers welfare and safety.</p>
                <p>We feel that what we do is important, that it matters and that it makes a difference.</p>
                <a class="tif-btn red mt-3" href="#more">Learn more</a>
            </div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial" id="more">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/ethics1.jpg" class="picture layr" data-rellax-speed="-0.5">
        <img src="images/ethics2.jpg" class="picture layr" data-rellax-speed="-1">
        <img src="images/ethics3.jpg" class="picture layr" data-rellax-speed="4">
        <div class="text-cont layr mt-5" data-rellax-speed="1">
            <p>The UK Insurance industry has not endeared itself over the years to consumers – We therefore understand at a time of vulnerability it’s hard for customers to put their trust in a financial services organisation – But whilst we are a Business, we are also Human</p>
        </div>
        <div class="text-cont layr mt-5" data-rellax-speed="0.4">
            <h4>'The moral insurance company'</h4>
        </div>
        <div class="text-cont layr mt-5" data-rellax-speed="3">
            <p>Sounds unlikely, but it’s what we do and have been doing for over twenty years. We operate differently, with very good reason and that is the best possible chance of good clinical outcome for the customer.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="banner layr" data-rellax-speed="1" style="background-image: url('images/ethics4.jpg');">
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-2"><h3>It’s a simple moral duty to do what is right.</h3></div>
    </div>
</section>

<section class="editorial reverse large-marge">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/ethics5.jpg" class="picture layr" data-rellax-speed="-0.5">
        <img src="images/ethics6.jpg" class="picture layr" data-rellax-speed="-1">
        <img src="images/ethics7.jpg" class="picture layr" data-rellax-speed="3">
        <div class="text-cont layr mt-5" data-rellax-speed="0.5">
            <p>As we have grown, we have witnessed first-hand immoral and unethical practice around the world is significant. This has made us passionate and determined to make a stand to change things.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p>We are not the only company in the industry that are aware of these issues, but we believe we are the only one currently prepared to tackle the problems</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="message quote" style="background-image: url('images/ethics8.jpg');">
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-2">
            <div class="paragraph mb-md-4">
                <h4>The path of least resistance leads to crooked rivers and crooked men</h4>
                <h5 class="source">- Henry David Thoreau</h5>
            </div><!-- end text -->
            <div class="clearfix"></div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="cardnav" id="more">
    <div class="container">
        <h4 class="underlined" style="margin-bottom: 7rem;">We have outlined Our Stance into key areas of concern, which are as follows:</h4>
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="panel layr" data-rellax-speed="3">
                    <div class="text-cont trans-slow">
                        <h5>Private Hospitals</h5>
                        <p>Corrupt practices; obscene overtreatment, risk of clinical harm and unlawful activity. We highlight our experiences and the action we are taking.</p>
                        <a class="tif-btn red mt-3" href="private-hospitals.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/private-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="panel layr" style="margin-top: 3rem;" data-rellax-speed="5">
                    <div class="text-cont trans-slow">
                        <h5>Air Ambulances</h5>
                        <p>It’s not always the answer. We outline the What, When and Why of an Air Ambulance.</p>
                        <a class="tif-btn red mt-3" href="air-ambulance.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/airlift-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-lg-4">
                <div class="panel layr" style="margin-top: 6rem;" data-rellax-speed="9">
                    <div class="text-cont trans-slow">
                        <h5>Premature Babies</h5>
                        <p>Timing is Everything. We discuss our Considerations for Premature Babies Abroad.</p>
                        <a class="tif-btn red mt-3" href="premature-babies.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/prem-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>

<section class="jumbo red" style="background-image: url('images/stories-hero.jpg');">
    <div class="shape"></div>
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">your stories</h2>
            <p>We have real case studies that are quite frankly horror stories. We want you to be aware of these situations, so you can at the very least choose to protect yourselves, whether you are insured by us or not.</p>
            <a class="tif-btn black mt-3" href="community.php#stories">Learn more</a><br>
            <a class="tif-btn mt-3" href="mailto:hello@tifgroup.co.uk">Tell us yours</a><br>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<?php include('footer.php'); ?>

</body>

</html>
