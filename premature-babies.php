<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TIF Group | Premature Babies</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link href="https://fonts.googleapis.com/css?family=Oswald:500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/master.css">
    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

        })(window,document,'script','dataLayer','GTM-P7RZ5VH');</script>

    <!-- End Google Tag Manager -->
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<?php include('header.php'); ?>

<section class="jumbo red black layr animated fadeIn">
    <a class="scroll-arrow trans" href="#more"><img src="images/scroll-arrow.png"></a><!-- end scroll arrow -->
    <div class="background" style="background-image: url('images/baby-hero.jpg');"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="-1">
            <h1 class="mb-3 animated fadeInDown">Premature<br>Babies</h1>
            <div class="animated fadeInUp">
                <p>A baby who is born before 37 weeks of pregnancy will be called a premature baby. The neonatal team have different words for different levels of premature birth. They may also use the word ‘preterm’ to talk about a baby being born early.</p>
                <a class="tif-btn red mt-3" href="#more">Learn more</a>
            </div>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial reverse align-right ambulance" id="more">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/baby1.jpg" class="picture layr" data-rellax-speed="2" style="top: 12rem;">
        <div class="text-cont layr" data-rellax-speed="1">
            <p>This is one of the most emotive and difficult situations that we ever have to deal with; a tiny baby who through no fault of their own, arrives early and a family who did not plan on having a baby on their holiday and so are completely unprepared for it.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p>The clinical complications can be significant, some may not present immediate issues for the baby, but poor timed repatriations of infants who may not have fully developed can result in longer term damage and cognitive issues as well as presenting an immediate risk to health.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="3">
            <p>Our consideration is firstly and simply, ensuring mother and baby are in an appropriate facility that can support the needs of a premature baby. Our second consideration, is the timing of when it is safe for baby to return back to the UK.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/baby2.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">Our decisions are considered extremely carefully in order to mitigate the risk of any harm coming to the baby.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/baby3.jpg" class="picture layr" data-rellax-speed="-0.5" style="top:25%;max-width: 500px;right: 3rem;">
        <div class="text-cont layr mt-2" data-rellax-speed="1.5">
            <p>Premature labour it is usually an emergency and unexpected and so the focus is around getting mum to a hospital, any hospital and as quickly as possible.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2">
            <p class="text-left pl-md-3 pl-xl-5">In some instances, we understand that this may be a private facility initially, who may well be able to handle an uncomplicated labour however in our experience they are unlikely to have the facilities to support an early baby requiring neo-natal care.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2.5">
            <p>In the event that mum and baby are in a private facility, our doctors will assess the clinical situation and where possible arrange a transfer safely to a state hospital that has a specific neo-natal intensive care unit so that there is access to the best doctors, facilities and range of specialisms should the baby suffer any complications as a result of their early arrival, which can be extremely complex.</p>
        </div>
        <div class="text-cont layr mt-2" data-rellax-speed="2.5">
            <p>In areas of the world where state facilities are not adequate the medical team will work to ascertain which privately funded facility could be best to support the medical situation.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/baby4.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">Premature Babies is a very specialist area of medicine</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial stance small-gap align-left">
    <div class="clearfix"></div>
    <div class="container">
        <img src="images/baby5.jpg" class="picture layr" data-rellax-speed="2" style="top: 37%;right: 4rem;">
        <div class="text-cont layr" data-rellax-speed="1">
            <p>We completely understand parents will desperately want to get home and we want to support that but only when it is safe to do so.  The risks associated with putting a premature baby whose organs and specifically lungs may have not fully developed in an aviation environment, is something we want to avoid at <span style="color:red;">all</span> costs.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p>An example of compilations that could occur is that, premature babies often suffer from anaemia due to the fact that their hematopoietic system (blood formation) is not mature yet. Due to the lack of oxygen (O2) that their body sends to their tissues, babies are put in an incubator and given extra oxygen.
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p>This can be done during flight and in a specific Air Ambulance however, the hyperoxygenation (higher than usual concentration of O2) may provoke the body to produce more blood vessels, specifically in their eyes that at aviation and increased pressure can then damage their ability to see long term. This is just one example of what may not present as an ‘immediate’ and critical outcome for the baby, but is a lifechanging effect of a poorly timed transfer.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h4 class="pl-5 text-left">Timing is everything.</h4>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p>When a baby is born prematurely, it can be expected that we, tifgroup, will not look to repatriate the baby by any method of transport before he or she is ‘term’, this does mean some parents have to stay in resort for some time, weeks even months. Depending on how early the baby is.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="callout" style="background-image: url('images/baby6.jpg');">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">OUR POSITION IS SIMPLY THAT WE DO NOT WANT TO RISK ANY EITHER IMMEDIATE OR LONGER-TERM COMPLICATIONS AS A RESULT OF RUSHING THE BABY BACK TO THE UK.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="editorial small-gap">
    <div class="clearfix"></div>
    <div class="container">
        <div class="text-cont layr" data-rellax-speed="1">
            <a class="tif-btn red mt-3" href="premature-babies.pdf" target="_blank">Read this as a PDF</a>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <h4 class="text-left">A <span style="color:red;">true</span> story</h4>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">Whilst in UAE a mother went into premature labour and gave birth by emergency c-section, to a baby girl at 26 weeks gestation, making her extremely pre-term.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">The little girl was born with and suffered, numerous complications of a perforated bowel, necrosing enterocolitis, ileostomy, ileostomy closure, haemorrhage into her cerebral ventricles, ventriculitis, hydrocephalus, with numerous procedures to release pressure by removing intracerebral fluid, and haemorrhage into lungs.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">We, <span style="color:red;">tifgroup</span>, recommended that the baby, who was not fit to be transferred back to the United Kingdom, should remain in resort where she was receiving excellent care.</p>
        </div>
        <div class="text-cont layr" data-rellax-speed="2">
            <p class="pl-0">She stayed in UAE for 3 months after she was born, after which she then required surgery to insert a ventricular peritoneal shunt from brain to abdomen. This treatment in UAE would have then required another stay of up to 4 months before she would be safe to transfer back to the UK. A clinical assessment was made as to whether the little girl was suitable for transfer by Air Ambulance pre-surgery and were advised that there was a small window of opportunity. This possibility was discussed with the parents who were understandably keen to return back to the United Kingdom and allowed us to safely repatriate them and their little girl to Birmingham Children’s Hospital to continue with her treatment.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<section class="cardnav">
    <div class="container">
        <h4 class="underlined" style="margin-bottom: 5rem;">Other key areas of concern:</h4>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-lg-5">
                <div class="panel">
                    <div class="text-cont trans-slow">
                        <h5>Private Hospitals</h5>
                        <p>Our position on where customers receive medical treatment is based solely on optimal care.</p>
                        <a class="tif-btn red mt-3" href="private-hospitals.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/private-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
            <div class="col-12 col-md-6 col-lg-5">
                <div class="panel">
                    <div class="text-cont trans-slow">
                        <h5>Air Ambulances</h5>
                        <p>It’s not always the answer. We outline the What, When and Why of an Air Ambulance.</p>
                        <a class="tif-btn red mt-3" href="air-ambulance.php">Learn more</a>
                    </div><!-- end text cont -->
                    <div class="picture trans" style="background-image: url('images/airlift-tall.jpg')"></div>
                </div><!-- end panel -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>

<section class="callout">
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-1">WE ARE NOT THE ONLY COMPANY IN THE INDUSTRY THAT ARE AWARE OF THESE ISSUES, BUT WE BELIEVE WE ARE THE ONLY ONE CURRENTLY PREPARED TO TACKLE THE PROBLEMS.</h2>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<section class="jumbo red" style="background-image: url('images/stories-hero.jpg');">
    <div class="shape"></div>
    <div class="container">
        <div class="text-cont">
            <h2 class="mb-3">your stories</h2>
            <p>We have real case studies that are quite frankly horror stories. We want you to be aware of these situations, so you can at the very least choose to protect yourselves, whether you are insured by us or not.</p>
            <a class="tif-btn black mt-3" href="community.php#stories">Learn more</a><br>
            <a class="tif-btn mt-3" href="mailto:hello@tifgroup.co.uk">Tell us yours</a><br>
        </div><!-- end text cont -->
    </div><!-- end container -->
</section>

<?php include('footer.php'); ?>

</body>

</html>
